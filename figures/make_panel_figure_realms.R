#Appendicex figures for all biotic and abiotic variables included in this study
source('~/Projects/is_australia_weird/figures/graphing_functions_realms.R', echo=FALSE)

terrestrial_processes_names<-c(expression(atop("GPP",paste(" (mg C ",m^-2, d^-1,")"))),
                               expression(atop("NPP",paste(" (mg C ",m^-2, d^-1,")"))))

terrestrial_processes_legend <- c("a Terrestrial gross primary productivity",
                                  "b Terrestrial net primary productivity")
terrestrial_processes_plots<-make.panel.figure("terrestrial_gpp_npp_data_realms",
                                               indices = 4:5,
                                               y.names = terrestrial_processes_names,
                                               log.t=TRUE,plot.type="box",add.to.title="",legend=FALSE,
                                               title = terrestrial_processes_legend)

seds_legend <- c("a Aluminium","b Barium","c Calcium","d Cobalt",
                 "e Chromium","f Copper","g Iron","h Potassium",
                 "i Magnesium","j Manganese","k Sodium","l Nickel",
                 "m Lead","n Scandium","o Strontium","p Thorium",
                 "q Titanium","r Uranium","s Vanadium","t Zinc")
seds_names<-c(expression(paste("Al (mg",.Kg^{-1},")")),
              expression(paste("Ba (mg",.Kg^{-1},")")),
              expression(paste("Ca (mg",.Kg^{-1},")")),
              expression(paste("Co (mg",.Kg^{-1},")")),
              expression(paste("Cr (mg",.Kg^{-1},")")),
              expression(paste("Cu (mg",.Kg^{-1},")")),
              expression(paste("Fe (mg",.Kg^{-1},")")),
              expression(paste("K (mg",.Kg^{-1},")")),
              expression(paste("Mg (mg",.Kg^{-1},")")),
              expression(paste("Mn (mg",.Kg^{-1},")")),
              expression(paste("Na (mg",.Kg^{-1},")")),
              expression(paste("Ni (mg",.Kg^{-1},")")),
              expression(paste("Pb (mg",.Kg^{-1},")")),
              expression(paste("Sc (mg",.Kg^{-1},")")),
              expression(paste("Sr (mg",.Kg^{-1},")")),
              expression(paste("Th (mg",.Kg^{-1},")")),
              expression(paste("Ti (mg",.Kg^{-1},")")),
              expression(paste("U (mg",.Kg^{-1},")")),
              expression(paste("V (mg",.Kg^{-1},")")),
              expression(paste("Zn (mg",.Kg^{-1},")")))

sed_boxplot<-make.panel.figure("seds_data_realms",indices = 4:23,y.names = seds_names,title = seds_legend,
                               log.t=TRUE,plot.type="box",add.to.title="",legend=FALSE)

soil_legend <- c("a Gravel",
                 "b Sand",
                 "c Silt",
                 "d Clay",
                 "e Bulk density",
                 "f Reference bulk\ndensity",
                 "g Organic Carbon",
                 "h pH",
                 "i Clay cation\nexchange capacity",
                 "j Soil cation\nexchange capacity",
                 "k Base saturation\nexchangeable cations",
                 "l Exchangeable\nbases",
                 "m Calcium\nCarbonate",
                 "n Gypsum",
                 "o Sodicity",
                 "p Salinity")

soil_names<-c("Gravel (% soil > 2mm)",
              "Sand (% wt)",
              "Silt (% wt)",
              "Clay (% wt)",
              expression(atop("Bulk density",paste("(Kg",.dm^{-3},")"))),
              expression(atop("Reference bulk density",paste("(Kg",.dm^{-3},")"))),
              "OC (% wt)",
              expression(atop("pH",paste("(-log",H^"+",")"))),
              expression(paste("Clay CEC (cmol ",.Kg^{-1},")")),
              expression(atop("Soil CEC",paste("(cmol ",.Kg^-1,")"))),
              "Base saturation (%)",
              expression(paste("TEB (cmol ",.Kg^-1,")")),
              expression(atop(paste("CaC",O^{3}),"(% wt)")),
              expression(atop(paste("CaS",O^{4}),"(% wt)")),
              "ESP (%)",
              expression(paste("ECe (dS",.m^{-1},")")))

soil_boxplot<-make.panel.figure("hwsd_soil_data_realms",indices = c(4:7,9,8,10:19),y.names = soil_names,title = soil_legend,
                                log.t=FALSE,plot.type="box",add.to.title="",legend=FALSE)

plant.names<-c(expression(paste("Leaf area ", (mm^{2}))),
               expression("SLA "(mm^2 %.% mg^-1)),
               "Seed mass (mg)",
               expression("Leaf N "(mg %.% g^-1)),
               expression("Leaf P "(mg %.% g^-1)),
               "Plant height (m)")
plant.legend <- c("a Leaf area",
                  "b Specific leaf area",
                  "c Seed mass",
                  "d Leaf nitrogen mass",
                  "e Leaf phosphorus mass",
                  "f Plant height")
plant_boxplots<-make.panel.figure("plant_data_realms",indices=c(3:8),y.names=plant.names,log.t=TRUE,plot.type="box",add.to.title="subplot",legend=FALSE,title = plant.legend)

plant.binary.names<-c("Proportion of\nC3 species",
                      "Proportion of animal\ndispersed species",
                      "Proportion of compounded\nleaved species",
                      "Proportion of\nN-fixing species",
                      "Proportion of\nevergreen species",
                      "Proportion of\nwoody species")
plant.legend.bin <- c("g Phothosynthethic pathway",
                      "h Dispersal syndrome",
                      "i Leaf type",
                      "j Nitrogen-fixing capacity",
                      "k Leaf phenology",
                      "l Growth form")

plant_prop_plots<-make.panel.figure("plant_data_realms",indices=c(9:10,12,11,13:14),y.names=plant.binary.names,log.t=FALSE,plot.type="bar",add.to.title="propsubplot",legend=FALSE,title = plant.legend.bin)


do.call(grid.arrange,  c(plant_boxplots,plant_prop_plots))
pdf(file="figures_output/plants_complete_realms.pdf",width=10,height=7)
do.call(grid.arrange,  c(plant_boxplots,plant_prop_plots))
dev.off()

climate.names<-c(expression(atop("Diurnal temperature","range "(degree*C))),
                 "Number days\nground-frost",
                 expression(atop("Precipitation",paste("mm ",.y^-1,")"))),
                 "Wet-days\n (No days > 0.1 mm ppt)")
climate.title <-c("a Diurnal temperature range",
                  "b Number of days ground-frost",
                  "c Mean annual\nprecipitation",
                  "d Number of wet days")
climate_1 <-make.panel.figure("terrestrial_climate_data_realms",c(3:6),y.names=climate.names,log.t=FALSE,plot.type="box",title = climate.title,legend = FALSE,add.to.title = "_1")

aridty.title <- "e Aridity"

aridity_name <- expression(atop("Aridity index",(over(MAP,PET))))

aridity <-make.panel.figure("aridity_data_realms",c(4),y.names=aridity_name,log.t=FALSE,plot.type="box",legend=FALSE,title = aridty.title,add.to.title = "subplot")  

climate.names_2 <-c("Sunshine percent\n(% daylenght)",
                    expression(atop("Mean temperature",(degree*C))),
                    "Number of rainy days driest\nmonth (d > 0.1 mm rain)",
                    "Interannual variation\nin precipitation (CV)",
                    "Mean wind\n(m/s)",
                    expression(atop("Maximum",paste("temperature ",(degree*C)))))
climate.title_2 <-c("f Sunshine percent",
                    "g Mean annual temperature",
                    "h Number of rainy days\nduring driest month",
                    "i Interannual variation\nin precipitation",
                    "j Mean wind speed",
                    "k Maximum temperature")

climate_2 <-make.panel.figure("terrestrial_climate_data_realms",c(8,9,12,11,10,13),y.names=climate.names_2,log.t=FALSE,plot.type="box",title = climate.title_2,legend = FALSE,add.to.title = "_2")

pdf(file="figures_output/terrestrial_climate_data_realms.pdf",width=10,height=7)
do.call(grid.arrange,  c(climate_1,aridity,climate_2))
dev.off()


decomp.names<-c(expression(atop(paste("k (g ",g^-1," ",y^-1,")"), "[log10-scale]")))
decomp.legend <- c("d Terrestrial litter decomposition")
decomp.plot<-make.panel.figure("decomposition_cornwell_data_realms",3,y.names=decomp.names,log.t=TRUE,plot.type="box",title = decomp.legend,legend = FALSE)

marine.names<-c(expression(atop("Mean sea surface Temp.",paste("(",degree*C,")"))),
                expression(atop("Mean nitrate concentration",paste(" (",mu,"mol ",l^-1," )"))),
                expression(atop("Mean phosphate concentration",paste(" (",mu,"mol ",l^-1," )"))),
                expression(atop("Mean silicate concentration",paste(" (",mu,"mol ",l^-1," )"))))
marine.legend <- c("a Sea surface temperature",
                   "b Nitrate concentration",
                   "c Phosphate concentration",
                   "d Silicate concentration")

make.panel.figure("marine_climate_data_realms",c(4,6:8),y.names=marine.names,log.t=FALSE,plot.type="box",legend = FALSE,title = marine.legend)

marine.herb.names<-c(expression(atop("Herbivory",paste("log( ", frac("Ungrazed biomass","Grazed biomass"),")"))))
herbivory.legend <- "e Marine herbivory"
marine.herb.plot<-make.panel.figure("marine_herbivory_data_realms",4,y.names=marine.herb.names,log.t=FALSE,plot.type="box",legend=FALSE,title = herbivory.legend)

marine.names<-c(expression(atop("NPP",paste(" (mg C ",m^-2, d^-1,")"))))
marine_produc <- c("c Marine net primary productivity")
marine.npp.plot<-make.panel.figure("marine_NPP_data_realms",4,y.names=marine.names,log.t=TRUE,plot.type="box",legend=FALSE,title = marine_produc)

fireleg <- expression(atop('Sum of fire events '[2001-2019],paste('fire events+1')))
fire <-make.panel.figure("fire_realms",c(4),y.names=fireleg,log.t=TRUE,plot.type="box",legend=FALSE,title = "f Fire events",add.to.title = "")

pdf(file="figures_output/ecosystem_processes.pdf",width=10,height=7)
do.call(grid.arrange,  c(terrestrial_processes_plots,marine.npp.plot,decomp.plot,marine.herb.plot,fire))
dev.off()

#soil N and P
soil_n_names <- expression(atop("Total N",paste("(g",.m^{-2},")")))
soil_n_title <- "e Total soil nitrogen"

soil_n_plot<-make.panel.figure("soil_total_n_data_realms",4,y.names=soil_n_names,log.t=TRUE,plot.type="box",legend=FALSE,title = soil_n_title,add.to.title = "subplot")

total_p_names <- expression(atop("Total P",paste("(mg",.kg^{-1},")")))
total_p <-make.panel.figure("soil_stp.0.10cm_he_data_realms",c(4),y.names=total_p_names,log.t=FALSE,plot.type="box",legend=FALSE,title = "a Total soil phosphorus",add.to.title = "")

soil_p_names <- c(#expression(atop("Total P",paste("(g",.m^{-2},")"))),
  expression(atop("Secondary P",paste("(g",.m^{-2},")"))),
  expression(atop("Organic P",paste("(g",.m^{-2},")"))),
  expression(atop("Labile inorganic P",paste("(g",.m^{-2},")"))))
soil_p_title <- c(#"a Total soil phosphorus",
  "b Soil secondary phosphorus",
  "c Soil organic phosphorus",
  "d Soil labile inorganic phosphorus")


soil_p_plot<-make.panel.figure("soil_total_p_data_realms",5:7,y.names=soil_p_names,log.t=TRUE,plot.type="box",legend=FALSE,title = soil_p_title,add.to.title = "subplot")

pdf(file="figures_output/soil_N_and_P_data_realms.pdf",width=10,height=7)
do.call(grid.arrange,  c(total_p,soil_p_plot,soil_n_plot))
dev.off()

#legends
library(tidyverse)
library(gridExtra)
terrestrial <- read_csv("data_for_graphs/aridity_data_realms.csv")
terrestrial$Realms <- factor(terrestrial$continent, c("Australian",
                                                      "Afrotropical",
                                                      "Nearctic",
                                                      "Neotropical",
                                                      "Oriental",
                                                      "Palearctic",
                                                      "Panamanian",
                                                      "Saharo-Arabian",
                                                      "Sino-Japanese"))
terrestrial$Realms <- droplevels(terrestrial$Realms)
terrestrial <- ggplot(terrestrial, aes(x= Realms, y =annual_aridity, fill = Realms))+
  geom_boxplot()+
  scale_fill_brewer(palette = "Spectral")+
  theme_classic(base_size = 20)+
  theme(legend.position = "bottom")
leg <- g_legend(terrestrial)
pdf(file = "figures_output/terrestrial_realms_legend.pdf",width = 10,height = 2)
grid.arrange(leg)
dev.off()

#marine realms legend
marine <- read_csv("data_for_graphs/marine_climate_data_realms.csv")
marine$Realms <- factor(marine$continent, c("Australia",
                                            "Artic seas",
                                            "Black sea",
                                            "Caribbean and Gulf of Mexico",
                                            "Chile",
                                            "Gulf of California",
                                            "Gulfs of Aqaba-Aden-Suez-Red Sea",
                                            "Indo-Pacific seas and Indian Ocean",
                                            "NW Pacific",
                                            "Inner baltic sea",
                                            "Mediterranean",
                                            "Mid-south Tropical Pacific",
                                            "Mid-tropical North Pacific Ocean",
                                            "N American Boreal",
                                            "N Pacific",
                                            "NE Atlantic",
                                            "New Zealand",
                                            "Norwegian sea",
                                            "Offshore and NW North Atlantic",
                                            "Offshore Indian Ocean",
                                            "Offshore mid-E Pacific",
                                            "Offshore S Atlantic",
                                            "Offshore W Pacific",
                                            "Rio de la Plata",
                                            "S Africa",
                                            "South-east Pacific",
                                            "Southern Ocean",
                                            "Tasman Sea",
                                            "Tropical E Atlantic"))

marine_p <- ggplot(marine, aes(x= Realms, y =mean_sst, fill = Realms))+
  geom_boxplot()+
  scale_fill_viridis_d()+
  theme_classic(base_size = 20)+
  theme(legend.position = "bottom")
leg_marine <- g_legend(marine_p)
pdf(file = "figures_output/marine_realms_legend.pdf",width = 10,height = 2)
grid.arrange(leg_marine)
dev.off()

