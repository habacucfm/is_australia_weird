#Appendicex figures for all biotic and abiotic variables included in this study
source("figures/graphing_functions.r")

terrestrial_processes_names<-c(expression(atop("GPP",paste(" (mg C ",m^-2, d^-1,")"))),
                               expression(atop("NPP",paste(" (mg C ",m^-2, d^-1,")"))))

terrestrial_processes_legend <- c("a Terrestrial gross primary productivity",
                                  "b Terrestrial net primary productivity")
terrestrial_processes_plots<-make.panel.figure("terrestrial_gpp_npp_data",
                                                indices = 4:5,
                                                y.names = terrestrial_processes_names,
                               log.t=TRUE,plot.type="box",add.to.title="",legend=FALSE,
                               title = terrestrial_processes_legend)

seds_legend <- c("a Aluminium","b Barium","c Calcium","d Cobalt",
                 "e Chromium","f Copper","g Iron","h Potassium",
                 "i Magnesium","j Manganese","k Sodium","l Nickel",
                 "m Lead","n Scandium","o Strontium","p Thorium",
                 "q Titanium","r Uranium","s Vanadium","t Zinc")
seds_names<-c(expression(paste("Al (mg",.Kg^{-1},")")),
              expression(paste("Ba (mg",.Kg^{-1},")")),
              expression(paste("Ca (mg",.Kg^{-1},")")),
              expression(paste("Co (mg",.Kg^{-1},")")),
              expression(paste("Cr (mg",.Kg^{-1},")")),
              expression(paste("Cu (mg",.Kg^{-1},")")),
              expression(paste("Fe (mg",.Kg^{-1},")")),
              expression(paste("K (mg",.Kg^{-1},")")),
              expression(paste("Mg (mg",.Kg^{-1},")")),
              expression(paste("Mn (mg",.Kg^{-1},")")),
              expression(paste("Na (mg",.Kg^{-1},")")),
              expression(paste("Ni (mg",.Kg^{-1},")")),
              expression(paste("Pb (mg",.Kg^{-1},")")),
              expression(paste("Sc (mg",.Kg^{-1},")")),
              expression(paste("Sr (mg",.Kg^{-1},")")),
              expression(paste("Th (mg",.Kg^{-1},")")),
              expression(paste("Ti (mg",.Kg^{-1},")")),
              expression(paste("U (mg",.Kg^{-1},")")),
              expression(paste("V (mg",.Kg^{-1},")")),
              expression(paste("Zn (mg",.Kg^{-1},")")))

sed_boxplot<-make.panel.figure("seds_data",indices = 5:24,y.names = seds_names,title = seds_legend,
                                log.t=TRUE,plot.type="box",add.to.title="",legend=TRUE)

soil_legend <- c("a Gravel",
                 "b Sand",
                 "c Silt",
                 "d Clay",
                 "e Bulk density",
                 "f Reference bulk\ndensity",
                 "g Organic Carbon",
                 "h pH",
                 "i Clay cation\nexchange capacity",
                 "j Soil cation\nexchange capacity",
                 "k Base saturation\nexchangeable cations",
                 "l Exchangeable\nbases",
                 "m Calcium\nCarbonate",
                 "n Gypsum",
                 "o Sodicity",
                 "p Salinity")
soil_names<-c("Gravel (% soil > 2mm)",
              "Sand (% wt)",
              "Silt (% wt)",
              "Clay (% wt)",
              expression(atop("Bulk density",paste("(Kg",.dm^{-3},")"))),
              expression(atop("Reference bulk density",paste("(Kg",.dm^{-3},")"))),
              "OC (% wt)",
              expression(atop("pH",paste("(-log",H^"+",")"))),
              expression(paste("Clay CEC (cmol ",.Kg^{-1},")")),
              expression(atop("Soil CEC",paste("(cmol ",.Kg^-1,")"))),
              "Base saturation (%)",
              expression(paste("TEB (cmol ",.Kg^-1,")")),
              expression(atop(paste("CaC",O^{3}),"(% wt)")),
              expression(atop(paste("CaS",O^{4}),"(% wt)")),
              "ESP (%)",
              expression(paste("ECe (dS",.m^{-1},")")))

soil_boxplot<-make.panel.figure("hwsd_soil_data",indices = c(4:7,9,8,10:19),y.names = soil_names,title = soil_legend,
                                log.t=FALSE,plot.type="box",add.to.title="",legend=TRUE)

plant.names<-c(expression(paste("Leaf area ", (mm^{2}))),
               expression("SLA "(mm^2 %.% mg^-1)),
               "Seed mass (mg)",
               expression("Leaf N "(mg %.% g^-1)),
               expression("Leaf P "(mg %.% g^-1)),
               "Plant height (m)")
plant.legend <- c("a Leaf area",
                 "b Specific leaf area",
                 "c Seed mass",
                 "d Leaf nitrogen mass",
                 "e Leaf phosphorus mass",
                 "f Plant height")
plant_boxplots<-make.panel.figure("plant_data",indices=c(3:8),y.names=plant.names,log.t=TRUE,plot.type="box",add.to.title="subplot",legend=FALSE,title = plant.legend)

plant.binary.names<-c("Proportion of\nC3 species",
                      "Proportion of animal\ndispersed species",
                      "Proportion of compounded\nleaved species",
                      "Proportion of\nN-fixing species",
                      "Proportion of\nevergreen species",
                      "Proportion of\nwoody species")
plant.legend.bin <- c("g Phothosynthethic pathway",
                  "h Dispersal syndrome",
                  "i Leaf type",
                  "j Nitrogen-fixing capacity",
                  "k Leaf phenology",
                  "l Growth form")

plant_prop_plots<-make.panel.figure("plant_data",indices=c(9:10,12,11,13:14),y.names=plant.binary.names,log.t=FALSE,plot.type="bar",add.to.title="propsubplot",legend=TRUE,title = plant.legend.bin)


#do.call(grid.arrange,  c(plant_boxplots,plant_prop_plots))
pdf(file="figures_output/plants_complete.pdf",width=10,height=7)
do.call(grid.arrange,  c(plant_boxplots,plant_prop_plots))
dev.off()

climate.names<-c(expression(atop("Diurnal temperature","range "(degree*C))),
                 "Number days\nground-frost",
                 expression(atop("Precipitation",paste("mm ",.y^-1,")"))),
                 "Wet-days\n (No days > 0.1 mm ppt)")
climate.title <-c("a Diurnal temperature\nrange",
                  "b Number of days\nground-frost",
                  "c Mean annual\nprecipitation",
                  "d Number of\nwet days")
climate_1 <-make.panel.figure("terrestrial_climate_data",c(4:7),y.names=climate.names,log.t=FALSE,plot.type="box",title = climate.title,legend = FALSE,add.to.title = "_1")

aridty.title <- "e Aridity"

aridity_name <- expression(atop("Aridity index",(over(MAP,PET))))

aridity <-make.panel.figure("aridity_data",c(4),y.names=aridity_name,log.t=FALSE,plot.type="box",legend=FALSE,title = aridty.title,add.to.title = "subplot")  

climate.names_2 <-c("Sunshine percent\n(% daylenght)",
                 expression(atop("Mean temperature",(degree*C))),
                 "Number of rainy days driest\nmonth (d > 0.1 mm rain)",
                 "Interannual variation\nin precipitation (CV)",
                 "Mean wind\n(m/s)",
                 expression(atop("Maximum",paste("temperature ",(degree*C)))))
climate.title_2 <-c("f Sunshine percent",
                  "g Mean annual\ntemperature",
                  "h Number of rainy\ndays during\ndriest month",
                  "i Interannual variation\nin precipitation",
                  "j Mean wind speed",
                  "k Maximum\ntemperature")

climate_2 <-make.panel.figure("terrestrial_climate_data",c(9,10,13,12,11,14),y.names=climate.names_2,log.t=FALSE,plot.type="box",title = climate.title_2,legend = TRUE,add.to.title = "_2")

pdf(file="figures_output/terrestrial_climate_data.pdf",width=10,height=7)
do.call(grid.arrange,  c(climate_1,aridity,climate_2))
dev.off()

bird.names<-c("Adult body mass\n(g)",
              "Age at maturity\n(d)",
              expression(atop("Posthatch growth rate (Posthacth",paste("weight (g)",.d^{-1},")"))),
              expression(atop("Production rate to hatching (Clutch size ",paste(".# clutches",.y^{-1},") .birth weight (g))"))),
              "Maximum lifespan\n(y)")

bird.legend <-c("g Bird adult body mass",
                "h Bird age at maturity",
                "i Bird posthatch growth rate",
                "j Bird production rate to hatching",
                "k Bird maximum lifespan")

birds<-make.panel.figure("birds_data",c(3:5,7,6),y.names=bird.names,log.t=FALSE,plot.type="box",title = bird.legend,legend = FALSE)

decomp.names<-c(expression(paste("k (g ",g^-1," ",y^-1,")")))
decomp.legend <- c("d Terrestrial litter decomposition")
decomp.plot<-make.panel.figure("decomposition_cornwell_data",3,y.names=decomp.names,log.t=TRUE,plot.type="box",title = decomp.legend,legend = FALSE)

m.names<-c("Adult body mass (g)",
           "Age at first\nreproduction (d)",
           "Maximum longevity\n(months)",
           expression(atop("Prod. rate to weaning",atop("(Litter size .# litters ",paste(y^{-1},". mass wean young)")))),
           expression(atop("Prod. rate to birth",atop("(Litter size .# litters ",paste(y^{-1},". mass neonate)")))),
           #expression(atop("Prod. rate to birth (Litter size .# litters ",paste(y^{-1},". mass wean young) [log10 scale]"))),
           expression(paste("BMR (ml ", O[2]," "," ",mm^2," "," ",g^-1,")")))
m.legends <- c("a Mammal adult body mass",
               "b Mammal age at first reproduction",
               "c Mammal maximum longevity",
               "d Mammal production rate to weaning",
               "e Mammal production rate to birth",
               "f Mammal basal metabolic rate")
mammals <-make.panel.figure("mammals_data",3:8,y.names=m.names,log.t=TRUE,plot.type="box",title = m.legends,legend = FALSE)

pdf(file="figures_output/vertebrate_data.pdf",width=13,height=9)
do.call(grid.arrange,  c(mammals,birds))
dev.off()


marine.names<-c(expression(atop("Mean sea surface Temp.",paste("(",degree*C,")"))),
                expression(atop("Mean nitrate concentration",paste(" (",mu,"mol ",l^-1," )"))),
                expression(atop("Mean phosphate concentration",paste(" (",mu,"mol ",l^-1," )"))),
                expression(atop("Mean silicate concentration",paste(" (",mu,"mol ",l^-1," )"))))
marine.legend <- c("a Sea surface temperature",
                   "b Nitrate concentration",
                   "c Phosphate concentration",
                   "d Silicate concentration")

make.panel.figure("marine_climate_data",c(4,6:8),y.names=marine.names,log.t=FALSE,plot.type="box",legend = TRUE,title = marine.legend)

marine.herb.names<-c(expression(atop("Herbivory",paste("log( ", frac("Ungrazed biomass","Grazed biomass"),")"))))
herbivory.legend <- "e Marine herbivory"
marine.herb.plot<-make.panel.figure("marine_herbivory_data",4,y.names=marine.herb.names,log.t=FALSE,plot.type="box",legend=FALSE,title = herbivory.legend)

marine.names<-c(expression(atop("NPP",paste(" (mg C ",m^-2, d^-1,")"))))
marine_produc <- c("c Marine net primary productivity")
marine.npp.plot<-make.panel.figure("marine_NPP_data",4,y.names=marine.names,log.t=TRUE,plot.type="box",legend=FALSE,title = marine_produc)

fireleg <- expression(atop('Sum of fire events '[2001-2019],paste('fire events+1')))
fire <-make.panel.figure("fire",c(4),y.names=fireleg,log.t=TRUE,plot.type="box",legend=TRUE,title = "f Fire events",add.to.title = "")

pdf(file="figures_output/continents_ecosystem_processes.pdf",width=10,height=7)
do.call(grid.arrange,  c(terrestrial_processes_plots,marine.npp.plot,decomp.plot,marine.herb.plot,fire))
dev.off()

#soil N and P
soil_n_names <- expression(atop("Total N",paste("(g",.m^{-2},")")))
soil_n_title <- "e Total soil nitrogen"

soil_n_plot<-make.panel.figure("soil_total_n_data",4,y.names=soil_n_names,log.t=TRUE,plot.type="box",legend=TRUE,title = soil_n_title,add.to.title = "subplot")

total_p_names <- expression(atop("Total P",paste("(mg",.kg^{-1},")")))
total_p <-make.panel.figure("soil_stp.0.10cm_he_data",c(4),y.names=total_p_names,log.t=FALSE,plot.type="box",legend=FALSE,title = "a Total soil phosphorus",add.to.title = "")


soil_p_names <- c(#expression(atop("Total P",paste("(g",.m^{-2},")"))),
                  expression(atop("Secondary P",paste("(g",.m^{-2},")"))),
                  expression(atop("Organic P",paste("(g",.m^{-2},")"))),
                  expression(atop("Labile inorganic P",paste("(g",.m^{-2},")"))))
soil_p_title <- c(#"a Total soil phosphorus",
                  "b Soil secondary phosphorus",
                  "c Soil organic phosphorus",
                  "d Soil labile inorganic phosphorus")

soil_p_plot<-make.panel.figure("soil_total_p_data",5:7,y.names=soil_p_names,log.t=TRUE,plot.type="box",legend=FALSE,title = soil_p_title,add.to.title = "subplot")

pdf(file="figures_output/soil_N_and_P_data.pdf",width=10,height=7)
do.call(grid.arrange,  c(total_p,soil_p_plot,soil_n_plot))
dev.off()
