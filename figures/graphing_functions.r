
cont.boxplot<-function(df,x_string,y_string,y_label,title,log=FALSE){ 
  p<-ggplot(na.omit(df[c(eval(y_string),x_string)]),aes_string( x = x_string, y = y_string))
  p<-p+geom_boxplot(aes(fill=Continent),notch = TRUE,outlier.size = 0.7)+
    ylab(y_label)+labs(title = title)+
    theme_classic(base_size = 12)+
    scale_fill_manual(values=c("Australia" = "#E41A1C",
                               "Africa" = "#377EB8",
                               "South America" = "#4DAF4A",
                               "Asia" = "#984EA3",
                               "North America" = "#FF7F00",
                               "Europe" = "#FFFF33"))
  p<-p+theme(axis.ticks.x=element_line(colour="black"),axis.text.x=element_blank(),
             axis.title.x=element_blank(),
             legend.position="none",
             plot.background=element_blank(),axis.line.x = element_line(color="black"),
             axis.line.y = element_line(color="black"))
  if(y_string%in%c("T_GRAVEL","T_SAND","T_SILT","T_CLAY","T_BS","sunp")) p<-p+scale_y_continuous(breaks = scales::breaks_extended(n=11))
  if(y_string%in%c("T_CACO3","T_CASO4","reh")) p<-p+scale_y_continuous(trans = "logit",
      n.breaks = 7,breaks =scales::breaks_pretty(7))
  if(y_string%in%c("T_ESP","T_OC","T_CEC_CLAY","T_CEC_SOIL","T_TEB","T_ECE","f_mass","aa_growth_rate",
                   "hatch_prod_g","pre","mean_nitrate","mean_phosphate","mean_silicate",
                   "adultbodymass_g","weaning_product_g","birth_prod_g","bmr_g")) p<-p+scale_y_log10(breaks = scales::breaks_log(n=7,base = 10))
  if(log==TRUE) p<-p+scale_y_log10(breaks = scales::breaks_log(n=7,base = 10))
  return(ggplotGrob(p))
}

#Extract Legend from a ggplot object
g_legend<-function(a.gplot){ 
  tmp <- ggplot_gtable(ggplot_build(a.gplot+theme(legend.position = "bottom"))) 
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box") 
  legend <- tmp$grobs[[leg]] 
  #grid.draw(legend) 
  return(legend)
} 


prop.barplot<-function(df,x_string,y_string,y_label,title=title){ 
  p<-ggplot(na.omit(df[c(eval(y_string),x_string)]),aes_string(x=x_string, y = y_string))
  p<-p+geom_bar(aes(fill=Continent),stat="identity",colour="black")+
    scale_y_continuous(breaks = scales::breaks_pretty(n=5))+
    ylab(y_label)+labs(title = title)+
    theme_classic(base_size = 12)+
    scale_fill_manual(values=c("Australia" = "#E41A1C",
                               "Africa" = "#377EB8",
                               "South America" = "#4DAF4A",
                               "Asia" = "#984EA3",
                               "North America" = "#FF7F00",
                               "Europe" = "#FFFF33"))
  p<-p+theme(axis.ticks.x=element_line(colour="black"),axis.text.x=element_blank(),
             axis.title.x=element_blank(),
             legend.position="none",
             plot.background=element_blank(),axis.line.x = element_line(color="black"),
             axis.line.y = element_line(color="black"))
  return(ggplotGrob(p))
}

point.plot<-function(df,x_string,y_string,std_string,y_label){ 
  require(ggplot2)
  require(grid)
  require(gridExtra)
  require(plyr)
  df$Continent <- factor(df$continent, c("Australia", "Africa", "South America", "Asia","North America","Europe"))
  yerr_names<-c(y_string,std_string)
  limits <- aes_string(ymin = paste(yerr_names, collapse = '-'), 
                       ymax = paste(yerr_names,collapse='+'))
  p<-ggplot(df,aes_string(colour="Continent",y=y_string,x="Continent"))
  p<-p+geom_pointrange(limits,size=1)+ylab(y_label)+theme_classic(base_size = 7)+scale_color_brewer(palette="Set1")
  p<-p+theme(axis.ticks.x=element_blank(),axis.text.x=element_blank(),
             axis.title.x=element_blank(),
             legend.position="none",
             plot.background=element_blank())
  return(ggplotGrob(p))
}




make.panel.figure<-function(raw.data,indices,y.names,title,legend=TRUE,log.t=FALSE,plot.type,add.to.title=""){
  require(ggplot2)
  require(grid)
  require(gridExtra)
  require(plyr)
  pd<-read.csv(paste0("data_for_graphs/",raw.data,".csv"))
  pd$Continent <- factor(pd$continent, c("Australia", "Africa", "South America", "Asia","North America","Europe"))
  trait.name.vec<-names(pd)[indices]
  if (plot.type=="box"){
    plots <- mapply(FUN=function(x,y,z)cont.boxplot(df=pd,x_string="Continent",y_string=x,y_label=y,title=z,log=log.t),
                    x=trait.name.vec,y=y.names,z=title,SIMPLIFY = FALSE)
  }
  if (plot.type=="bar"){
    df<-ddply(pd,.(Continent),numcolwise(mean),na.rm=T)
    plots <- mapply(FUN=function(x,y,z)prop.barplot(df=df,x_string="Continent",y_string=x,y_label=y,title=z),
                    x=trait.name.vec,y=y.names,z=title,SIMPLIFY = FALSE)
  }
  if(legend){
    plots.p<-ggplot(pd,aes_string(x="Continent",y=trait.name.vec[1]))+geom_boxplot(aes(fill=Continent))+
      scale_fill_brewer(palette="Set1")+ theme_classic(base_size = 18)
    le<-g_legend(plots.p)
    plots<-c(plots,list(NULL,NULL,le))
  }
  pdf(file=sprintf("figures_output/%s%s.pdf",raw.data,add.to.title),width=12,height=7.5)
    do.call(grid.arrange,  plots)
  dev.off()
  return(plots)
}

#code for logit transformation
library(scales)
logit_trans <- trans_new("inverse logit",
                         transform = car::logit,
                         inverse = boot::inv.logit)
scaleFUN <- function(x) sprintf("%.2f", x)
