# Is Australia weird? A cross-continental comparison of biological, geological and climatological features

###### Habacuc Flores-Moreno, Gordana Popovic, Will K Cornwell, Rhiannon L Dalrymple, Shinichi Nakagawa, Shawn W Laffan, Julia Cooke, Stephen P Bonser, Lisa E Schwanz, Angela J Crean, David J Eldridge, Michael Garratt, Robert C Brooks, Adriana Vergés, Alistair GB Poore, David R Cohen, Graeme F Clark, Alex Sen Gupta, Peter B Reich, J. Hans C. Cornelissen, Joseph M. Craine, Frank A. Hemmings, Jens Kattge, Ülo Niinemets, Josep Peñuelas, Angela T Moles

This repository contains all the code for the [Is Australia weird?](https://https://bitbucket.org/habacucfm/is_australia_weir) project. This idea was first developed during a Meta analysis and data compilation in Ecology working group ran by Habacuc Flores-Moreno and Angela Moles and supported by the Evolution & Ecology Research Centre UNSW.

You can find the statistical analyses for this project under the __analyses__ directory. The __data_wrangling__ directory contains the data processing steps that were taken for each dataset. The __figures__ directory contains all the scripts used to create the graphs for this paper, you can find all the figures under __figure_output__. The __r_output__ directory contains the output files for analyses that took long to run (> 1 hr).

Data used for this project are publicly available:


* [terrestrial climate](https://crudata.uea.ac.uk/cru/data/hrg/)


* [marine climate](https://www.ncdc.noaa.gov/oisst)


* [marine nutrients](https://www.nodc.noaa.gov/OC5/WOA09/woa09data.html)


* [marine productivity](http://www.science.oregonstate.edu/ocean.productivity)


* stream sediment geochemistry ([here](http://www.ga.gov.au/about/projects/resources/national-geochemical-survey), [here](http://www.cprm.gov.br/en/Geology/Geochemistry-513.html), [here](http://weppi.gtk.fi/publ/foregsatlas/ForegsData.php), and [here](; https://mrdata.usgs.gov/nure/sediment/))


* [soil edaphic properties](http://webarchive.iiasa.ac.at/Research/LUC/External-World-soil-database/)


* [soil phosphorus](https://daac.ornl.gov/SOILS/guides/Global_Phosphorus_Dist_Map.html)


* [soil nitrogen](http://www.isric.org/data/isric-wise-global-soil-profile-data-ver-31)


* [plants and decomposition](www.try-db.org)

* [mammals](http://onlinelibrary.wiley.com/doi/10.1890/08-1494.1/abstract)


* birds [here](http://onlinelibrary.wiley.com/doi/10.1890/06-2054/full), and also [here](http://genomics.senescence.info/species/))


* [terrestrial GPP and NPP](https://lpdaac.usgs.gov/dataset_discovery/modis/modis_products_table/mod17a3)
