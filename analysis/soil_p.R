library(nlme)
library(lme4)
library(MCMCglmm)
library(spBayes)


soil_p=read.csv("data_for_analysis/soil_total_p_data.csv",header=T)



# soil_p=soil_p[1:167641,]
colnames(soil_p)[1:3]=c("continent","Long", "Lat")
datcols=c(4:7)
soil_p$continent=factor(soil_p$continent)
soil_p$aust=soil_p$continent=="Australia" 
vars=colnames(soil_p[datcols])
Type=rep(c("spherical", "exponential", "gaussian", "linear","rational"),2)
Nug=rep(c(TRUE,FALSE),each=5)

#total_p
soil_p.marg=soil_p[!is.na(soil_p$total_p),]

k=ceiling(sqrt(dim(soil_p.marg)[1]/500))
n.lat=length(unique(soil_p.marg$Lat))
lat.sp=sort(unique(soil_p.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(soil_p.marg$Long))
long.sp=sort(unique(soil_p.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
soil_p.marg.sp=soil_p.marg[soil_p.marg$Lat%in%lat.sp&soil_p.marg$Long%in%long.sp,]#subsample
boxplot(soil_p.marg.sp$total_p~soil_p.marg.sp$continent)



AIC_total_p=rep(NA,10)
for(k in 1:10){
  AIC_total_p[k]=try(AIC(lme(fixed = total_p ~ -1+aust, data = soil_p.marg.sp,
                              random = ~ 1 | continent,
                              correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                              method = "ML")));
  if(class(AIC_total_p[k]) == "try-error") next;
}
save(AIC_total_p,file="r_output/AIC_total_p")
best=which(AIC_total_p==min(AIC_total_p))
c(Type[best],Nug[best])





#total_p

soil_p.marg=soil_p[!is.na(soil_p$total_p),]

k=ceiling(sqrt(dim(soil_p.marg)[1]/2000))
n.lat=length(unique(soil_p.marg$Lat))
lat.sp=sort(unique(soil_p.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(soil_p.marg$Long))
long.sp=sort(unique(soil_p.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
soil_p.marg.sp=soil_p.marg[soil_p.marg$Lat%in%lat.sp&soil_p.marg$Long%in%long.sp,]#subsample
boxplot(soil_p.marg.sp$total_p~soil_p.marg.sp$continent)

best=which(AIC_total_p==min(AIC_total_p))

total_p.null <- lme(fixed = total_p  ~ 1, data = soil_p.marg.sp, random = ~ 1 | continent,
                     correlation = corSpatial( form = ~ Lat + Long| continent,
                                               nugget=Nug[best],type=Type[best]),
                     method = "ML")




save(total_p.null , file="r_output/total_p_null")

total_p.alt=lme(fixed = total_p  ~ -1+aust, data = soil_p.marg.sp,
                 random = ~ 1 | continent,
                 correlation = corSpatial( form = ~ Lat + Long| continent,
                                           nugget=Nug[best],type=Type[best]),
                 method = "ML")




save(total_p.alt , file="r_output/total_p_alt")

anova(total_p.null,total_p.alt)# 0.8628

load(file="r_output/total_p_null")
plot(total_p.null)
load(file="r_output/total_p_alt")
plot(total_p.alt)
t(round(AICc(total_p.null,total_p.alt),2))
#secondary_p
soil_p.marg=soil_p[!is.na(soil_p$secondary_p),]

k=ceiling(sqrt(dim(soil_p.marg)[1]/500))
n.lat=length(unique(soil_p.marg$Lat))
lat.sp=sort(unique(soil_p.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(soil_p.marg$Long))
long.sp=sort(unique(soil_p.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
soil_p.marg.sp=soil_p.marg[soil_p.marg$Lat%in%lat.sp&soil_p.marg$Long%in%long.sp,]#subsample
boxplot(soil_p.marg.sp$secondary_p~soil_p.marg.sp$continent)



AIC_secondary_p=rep(NA,10)
for(k in 1:10){
  AIC_secondary_p[k]=try(AIC(lme(fixed = secondary_p ~ -1+aust, data = soil_p.marg.sp,
                                 random = ~ 1 | continent,
                                 correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                                 method = "ML")));
  if(class(AIC_secondary_p[k]) == "try-error") next;
}
save(AIC_secondary_p,file="r_output/AIC_secondary_p")
best=which(AIC_secondary_p==min(AIC_secondary_p))
c(Type[best],Nug[best])





#secondary_p

soil_p.marg=soil_p[!is.na(soil_p$secondary_p),]

k=ceiling(sqrt(dim(soil_p.marg)[1]/2000))
n.lat=length(unique(soil_p.marg$Lat))
lat.sp=sort(unique(soil_p.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(soil_p.marg$Long))
long.sp=sort(unique(soil_p.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
soil_p.marg.sp=soil_p.marg[soil_p.marg$Lat%in%lat.sp&soil_p.marg$Long%in%long.sp,]#subsample
boxplot(soil_p.marg.sp$secondary_p~soil_p.marg.sp$continent)

best=which(AIC_secondary_p==min(AIC_secondary_p))

secondary_p.null <- lme(fixed = secondary_p  ~ 1, data = soil_p.marg.sp, random = ~ 1 | continent,
                        correlation = corSpatial( form = ~ Lat + Long| continent,
                                                  nugget=Nug[best],type=Type[best]),
                        method = "ML")




save(secondary_p.null , file="r_output/secondary_p_null")

secondary_p.alt=lme(fixed = secondary_p  ~ -1+aust, data = soil_p.marg.sp,
                    random = ~ 1 | continent,
                    correlation = corSpatial( form = ~ Lat + Long| continent,
                                              nugget=Nug[best],type=Type[best]),
                    method = "ML")




save(secondary_p.alt , file="r_output/secondary_p_alt")

anova(secondary_p.null,secondary_p.alt)# 0.289

load(file="r_output/secondary_p_null")
plot(secondary_p.null)
load(file="r_output/secondary_p_alt")
plot(secondary_p.alt)
t(round(AICc(secondary_p.null,secondary_p.alt),2))
#organic_p
soil_p.marg=soil_p[!is.na(soil_p$organic_p),]

k=ceiling(sqrt(dim(soil_p.marg)[1]/500))
n.lat=length(unique(soil_p.marg$Lat))
lat.sp=sort(unique(soil_p.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(soil_p.marg$Long))
long.sp=sort(unique(soil_p.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
soil_p.marg.sp=soil_p.marg[soil_p.marg$Lat%in%lat.sp&soil_p.marg$Long%in%long.sp,]#subsample
boxplot(soil_p.marg.sp$organic_p~soil_p.marg.sp$continent)



AIC_organic_p=rep(NA,10)
for(k in 1:10){
  AIC_organic_p[k]=try(AIC(lme(fixed = organic_p ~ -1+aust, data = soil_p.marg.sp,
                               random = ~ 1 | continent,
                               correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                               method = "ML")));
  if(class(AIC_organic_p[k]) == "try-error") next;
}
save(AIC_organic_p,file="r_output/AIC_organic_p")
best=which(AIC_organic_p==min(AIC_organic_p))
c(Type[best],Nug[best])





#organic_p

soil_p.marg=soil_p[!is.na(soil_p$organic_p),]

k=ceiling(sqrt(dim(soil_p.marg)[1]/2000))
n.lat=length(unique(soil_p.marg$Lat))
lat.sp=sort(unique(soil_p.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(soil_p.marg$Long))
long.sp=sort(unique(soil_p.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
soil_p.marg.sp=soil_p.marg[soil_p.marg$Lat%in%lat.sp&soil_p.marg$Long%in%long.sp,]#subsample
boxplot(soil_p.marg.sp$organic_p~soil_p.marg.sp$continent)

best=which(AIC_organic_p==min(AIC_organic_p))

organic_p.null <- lme(fixed = organic_p  ~ 1, data = soil_p.marg.sp, random = ~ 1 | continent,
                      correlation = corSpatial( form = ~ Lat + Long| continent,
                                                nugget=Nug[best],type=Type[best]),
                      method = "ML")




save(organic_p.null , file="r_output/organic_p_null")

organic_p.alt=lme(fixed = organic_p  ~ -1+aust, data = soil_p.marg.sp,
                  random = ~ 1 | continent,
                  correlation = corSpatial( form = ~ Lat + Long| continent,
                                            nugget=Nug[best],type=Type[best]),
                  method = "ML")




save(organic_p.alt , file="r_output/organic_p_alt")

anova(organic_p.null,organic_p.alt)# 0.1562

load(file="r_output/organic_p_null")
plot(organic_p.null)
load(file="r_output/organic_p_alt")
plot(organic_p.alt)
t(round(AICc(organic_p.null,organic_p.alt),2))
#lab_inorg_p
soil_p.marg=soil_p[!is.na(soil_p$lab_inorg_p),]

k=ceiling(sqrt(dim(soil_p.marg)[1]/500))
n.lat=length(unique(soil_p.marg$Lat))
lat.sp=sort(unique(soil_p.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(soil_p.marg$Long))
long.sp=sort(unique(soil_p.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
soil_p.marg.sp=soil_p.marg[soil_p.marg$Lat%in%lat.sp&soil_p.marg$Long%in%long.sp,]#subsample
boxplot(log(soil_p.marg.sp$lab_inorg_p+1)~soil_p.marg.sp$continent)



AIC_lab_inorg_p=rep(NA,10)
for(k in 1:10){
  AIC_lab_inorg_p[k]=try(AIC(lme(fixed = lab_inorg_p ~ -1+aust, data = soil_p.marg.sp,
                                 random = ~ 1 | continent,
                                 correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                                 method = "ML")));
  if(class(AIC_lab_inorg_p[k]) == "try-error") next;
}
save(AIC_lab_inorg_p,file="r_output/AIC_lab_inorg_p")
best=which(AIC_lab_inorg_p==min(AIC_lab_inorg_p))
c(Type[best],Nug[best])





#lab_inorg_p

soil_p.marg=soil_p[!is.na(soil_p$lab_inorg_p),]

k=ceiling(sqrt(dim(soil_p.marg)[1]/2000))
n.lat=length(unique(soil_p.marg$Lat))
lat.sp=sort(unique(soil_p.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(soil_p.marg$Long))
long.sp=sort(unique(soil_p.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
soil_p.marg.sp=soil_p.marg[soil_p.marg$Lat%in%lat.sp&soil_p.marg$Long%in%long.sp,]#subsample
boxplot(soil_p.marg.sp$lab_inorg_p~soil_p.marg.sp$continent)

best=which(AIC_lab_inorg_p==min(AIC_lab_inorg_p))

lab_inorg_p.null <- lme(fixed = lab_inorg_p  ~ 1, data = soil_p.marg.sp, random = ~ 1 | continent,
                        correlation = corSpatial( form = ~ Lat + Long| continent,
                                                  nugget=Nug[best],type=Type[best]),
                        method = "ML")




save(lab_inorg_p.null , file="r_output/lab_inorg_p_null")

lab_inorg_p.alt=lme(fixed = lab_inorg_p  ~ -1+aust, data = soil_p.marg.sp,
                    random = ~ 1 | continent,
                    correlation = corSpatial( form = ~ Lat + Long| continent,
                                              nugget=Nug[best],type=Type[best]),
                    method = "ML")




save(lab_inorg_p.alt , file="r_output/lab_inorg_p_alt")

anova(lab_inorg_p.null,lab_inorg_p.alt)# 0.5973

load(file="r_output/lab_inorg_p_null")
plot(lab_inorg_p.null)
load(file="r_output/lab_inorg_p_alt")
plot(lab_inorg_p.alt)

t(round(AICc(lab_inorg_p.null,lab_inorg_p.alt),2))
