## Check to see if a vector is integer (or close enough to integer).
## If error is FALSE, this will return NULL
check.integer <- function(x, error=TRUE) {
  nna <- !is.na(x)
  if (max(abs(x[nna] - round(x[nna]))) > 1e-08)
    if ( error )
      stop("Non-integer argument for ", deparse(substitute(x)))
  else
    return(NULL)
  storage.mode(x) <- "integer"
  x
}
#compute distinctiveness analysis following https://github.com/traitecoevo/ksi
#see here https://doi.org/10.1111/1365-2745.12208
ksi_test <-  function(dat, group, var, factor, test = NULL) {
  dat <- dat[!is.na(dat[[var]]),]
  d.n <- dat[var][dat[[factor]] == group,]
  #print(d.n)
  d.t <- dat[var][dat[[factor]] != group,]
  n <- c(length(d.n), length(d.t))
#  print(n)
  if ( is.null(test) ) {
  if ( is.logical(d.t) || is.factor(d.t) ||
       !is.null(check.integer(d.t, FALSE)) ) {
    m <- rbind(neighbourhood=table(d.n),
               target=table(d.t))
    fit <- suppressWarnings(chisq.test(m))
    stat <- fit$statistic
    
  } else if (is.numeric(d.t)){
  n1n2 <- sqrt(prod(n)/sum(n))
#  print(n1n2)
  fit <- suppressWarnings(ks.test(d.n, d.t))
    stat <- fit$statistic * n1n2
    
      
  }
  } else {
    test <- "ks"
    n1n2 <- sqrt(prod(n)/sum(n))
    #  print(n1n2)
    fit <- suppressWarnings(ks.test(d.n, d.t))
    stat <- fit$statistic * n1n2
    
  }
  
  list(statistic = as.numeric(stat),
       method = fit$method,
       p.value   = fit$p.value,
       nf         = n[1],
       nbase = n[2],
       fit       = fit
       )
  
}


summary.ksi <- function(object, ...) {
  # nodesets <- sapply(seq_along(object), function(i)
  #   paste(ksi.nodeset(object, i), collapse="; "))
  stat <- sapply(object, "[[", "statistic")
  ret <- data.frame(node=names(object),
                    #rank=seq_along(object),
                    statistic=stat,
                    statistic.rel=stat / max(stat),
                    # nodesets=nodesets,
                    stringsAsFactors=FALSE)
  ret <- ret[order(ret$statistic.rel,decreasing = TRUE),]
  ret$rank <- seq_along(object)
  rownames(ret) <- NULL
  ret
}

