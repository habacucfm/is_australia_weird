#read in data
library(nlme)
library(lme4)
library(MCMCglmm)
library(tidyverse)
#Seds


seds=read.csv("data_for_analysis/seds_data_realms.csv")
seds %>%
  select(sample, lat, lon, continent, everything())-> seds
#plot data
datcols=5:24   #5:24



#tranform data into columns
n.var=length(datcols)
continent=factor(rep(seds$continent,n.var))
variable=factor(rep(colnames(seds)[datcols],each=dim(seds)[1]))
aust=continent=="Australian"
id=rep(1:dim(seds)[1],n.var)
value=as.vector(as.matrix(seds[,datcols]))
strsed=data.frame(value=value,aust=aust,variable=variable,continent=continent,id=id)
strsed=strsed[!is.na(strsed$value),] #we remove missing data
strsed$v <- as.numeric(strsed$variable) #we need this later for lme



#univariate models for strsedda data

#"al"

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[1],]
boxplot(strsed.marg$value~strsed.marg$continent)
#boxplot(log(strsed.marg$value)~strsed.marg$continent)
marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(value ~ aust , random = ~ 1| continent , data = strsed.marg,method="ML")
summary(marg.null )
summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.1421



#"ba" #log tr

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[2],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value)~strsed.marg$continent)
marg.null<- lme(log(value) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value) ~ aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.0493

#"ca" #log tr

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[3],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value)~strsed.marg$continent)
marg.null<- lme(log(value) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value) ~ aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.0103


#"co" #log tr

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[4],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value)~strsed.marg$continent)
#marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
#marg.alt<- lme(value ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.null<- lme(log(value) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value) ~ aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.2081



#"cr" #log tr

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[5],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value)~strsed.marg$continent)
#marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
#marg.alt<- lme(value ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.null<- lme(log(value) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value) ~ aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.9118





#"cu" #log tr

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[6],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value)~strsed.marg$continent)
#marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
#marg.alt<- lme(value ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.null<- lme(log(value) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=8e-04 Aus = 2.504422, Aus F = 2.859029



#"fe" #log tr

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[7],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value)~strsed.marg$continent)
#marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
#marg.alt<- lme(value ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.null<- lme(log(value) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value) ~ aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.1415



#"k" #untr

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[8],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value)~strsed.marg$continent)
marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(value ~ aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.0165



#"mg" #log+1    

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[9],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
marg.null<- lme(log(value+1) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value+1) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.0017



#"mn" #log+1

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[10],]
strsed.marg=strsed.marg[-which(strsed.marg$value==min(strsed.marg$value)),]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
marg.null<- lme(log(value+1) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value+1) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p= < 0.0001



#"na" #log+1  

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[11],]
strsed.marg=strsed.marg[-which(strsed.marg$value==min(strsed.marg$value)),]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
marg.null<- lme(log(value+1) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value+1) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p = 1e-04



#"ni" #log+1  

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[12],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
marg.null<- lme(log(value+1) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value+1) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.6872


#"pb" #log+1  

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[13],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
marg.null<- lme(log(value+1) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value+1) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.5436



#"sc" #log+1  

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[14],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
marg.null<- lme(log(value+1) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value+1) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.8431




#"sr" #log+1  

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[15],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
marg.null<- lme(log(value+1) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value+1) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.0211




#"th" #log+1  

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[16],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
#marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
#marg.alt<- lme(value ~ aust , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.null<- lme(log(value) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK 
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.5557


#"ti" #log+1  

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[17],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
marg.null<- lme(log(value+1) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value+1) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK 
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.3476





#"u" #log+1  

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[18],]
boxplot(strsed.marg$value~strsed.marg$continent)
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
marg.null<- lme(log(value+1) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value+1) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK 
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.0391



#"v" #log+1  

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[19],]
strsed.marg=strsed.marg[-which(strsed.marg$value==max(strsed.marg$value)),]
boxplot(strsed.marg$value~strsed.marg$continent) 
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
marg.null<- lme(log(value+1) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value+1) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK 
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.2085


#"zn" #log+1 

strsed.marg=strsed[strsed$variable==levels(strsed$variable)[20],]
boxplot(strsed.marg$value~strsed.marg$continent) 
boxplot(log(strsed.marg$value+1)~strsed.marg$continent)
marg.null<- lme(log(value+1) ~ 1 , random = ~ 1| continent , data = strsed.marg,method="ML")
marg.alt<- lme(log(value+1) ~ -1+aust , random = ~ 1| continent , data = strsed.marg,method="ML")
#summary(marg.null )
#summary(marg.alt )

plot(marg.null ) #OK 
plot(marg.alt ) #OK
anova(marg.null,marg.alt)
t(round(AICc(marg.null,marg.alt),2)) #p=0.0272
