library(nlme)
library(lme4)
library(MCMCglmm)
library(spBayes)





aridity=read.csv("data_for_analysis/aridity_data_realms.csv",header=T)
# aridity=aridity[1:167641,]
colnames(aridity)[1:3]=c("continent","Lat", "Long")
datcols=c(4)
aridity$continent=factor(aridity$continent)
aridity$aust=aridity$continent=="Australian" 

vars=colnames(aridity[datcols])
Type=rep(c("spherical", "exponential", "gaussian", "linear","rational"),2)
Nug=rep(c(TRUE,FALSE),each=5)

#univariate
#annual_aridity
aridity.marg=aridity[!is.na(aridity$annual_aridity),]

k=ceiling(sqrt(dim(aridity.marg)[1]/500))
n.lat=length(unique(aridity.marg$Lat))
lat.sp=sort(unique(aridity.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(aridity.marg$Long))
long.sp=sort(unique(aridity.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
aridity.marg.sp=aridity.marg[aridity.marg$Lat%in%lat.sp&aridity.marg$Long%in%long.sp,]#subsample
boxplot(aridity.marg.sp$annual_aridity~aridity.marg.sp$continent)



AIC_annual_aridity=rep(NA,10)
for(k in 1:10){
  AIC_annual_aridity[k]=try(AIC(lme(fixed = annual_aridity ~ -1+aust, data = aridity.marg.sp,
                                    random = ~ 1 | continent,
                                    correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                                    method = "ML")));
  if(class(AIC_annual_aridity[k]) == "try-error") next;
}
save(AIC_annual_aridity,file="r_output/AIC_aridity_realms")
best=which(AIC_annual_aridity==min(AIC_annual_aridity))
c(Type[best],Nug[best])

#aridity

aridity.marg=aridity[!is.na(aridity$annual_aridity),]

k=ceiling(sqrt(dim(aridity.marg)[1]/2000))
n.lat=length(unique(aridity.marg$Lat))
lat.sp=sort(unique(aridity.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(aridity.marg$Long))
long.sp=sort(unique(aridity.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
aridity.marg.sp=aridity.marg[aridity.marg$Lat%in%lat.sp&aridity.marg$Long%in%long.sp,]#subsample
boxplot(aridity.marg.sp$annual_aridity~aridity.marg.sp$continent)

best=which(AIC_annual_aridity==min(AIC_annual_aridity))

aridity.null <- lme(fixed = annual_aridity  ~ 1, data = aridity.marg.sp, random = ~ 1 | continent,
                    correlation = corSpatial( form = ~ Lat + Long| continent,
                                              nugget=Nug[best],type=Type[best]),
                    method = "ML")




save(aridity.null , file="r_output/aridity_null_realms")

aridity.alt=lme(fixed = annual_aridity  ~ -1+aust, data = aridity.marg.sp,
                random = ~ 1 | continent,
                correlation = corSpatial( form = ~ Lat + Long| continent,
                                          nugget=Nug[best],type=Type[best]),
                method = "ML")




save(aridity.alt , file="r_output/aridity_alt_realms")

anova(aridity.null,aridity.alt)# 0.4563

load(file="r_output/aridity_null_realms")
plot(aridity.null)
load(file="r_output/aridity_alt_realms")
plot(aridity.alt)

t(round(AICc(aridity.null,aridity.alt),2))
