library(nlme)
library(lme4)
library(MCMCglmm)
library(spBayes)


ter_prod=read.csv("data_for_analysis/terrestrial_gpp_npp_data.csv",header=T)
colnames(ter_prod)[1:3]=c("continent", "Long","Lat")
datcols=c(4:5)
ter_prod$continent=factor(ter_prod$continent)
ter_prod$aust=ter_prod$continent=="Australia"


vars=colnames(ter_prod[datcols])
Type=rep(c("spherical", "exponential", "gaussian", "linear","rational"),2)
Nug=rep(c(TRUE,FALSE),each=5)

#univariate
#gpp
ter_prod.marg=ter_prod[!is.na(ter_prod$gpp),]

k=ceiling(sqrt(dim(ter_prod.marg)[1]/500))
n.lat=length(unique(ter_prod.marg$Lat))
lat.sp=sort(unique(ter_prod.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(ter_prod.marg$Long))
long.sp=sort(unique(ter_prod.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
ter_prod.marg.sp=ter_prod.marg[ter_prod.marg$Lat%in%lat.sp&ter_prod.marg$Long%in%long.sp,]#subsample
boxplot(ter_prod.marg.sp$gpp~ter_prod.marg.sp$continent)



AIC_gpp=rep(NA,10)
for(k in 1:10){
  AIC_gpp[k]=try(AIC(lme(fixed = gpp ~ -1+aust, data = ter_prod.marg.sp,
                         random = ~ 1 | continent,
                         correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                         method = "ML")));
  if(class(AIC_gpp[k]) == "try-error") next;
}
save(AIC_gpp,file="~/is_australia_weird/r_output/AIC_gpp")
best=which(AIC_gpp==min(AIC_gpp))
c(Type[best],Nug[best])

#gpp

ter_prod.marg=ter_prod[!is.na(ter_prod$gpp),]

k=ceiling(sqrt(dim(ter_prod.marg)[1]/10000))
n.lat=length(unique(ter_prod.marg$Lat))
lat.sp=sort(unique(ter_prod.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(ter_prod.marg$Long))
long.sp=sort(unique(ter_prod.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
ter_prod.marg.sp=ter_prod.marg[ter_prod.marg$Lat%in%lat.sp&ter_prod.marg$Long%in%long.sp,]#subsample
boxplot(ter_prod.marg.sp$gpp~ter_prod.marg.sp$continent)

best=which(AIC_gpp==min(AIC_gpp))

gpp.null <- lme(fixed = gpp  ~ 1, data = ter_prod.marg.sp, random = ~ 1 | continent,
                method = "ML")




save(gpp.null , file="~/is_australia_weird/r_output/gpp_null")

gpp.alt=lme(fixed = gpp  ~ -1+aust, data = ter_prod.marg.sp,
            random = ~ 1 | continent,
            method = "ML")




save(gpp.alt , file="~/is_australia_weird/r_output/gpp_alt")

anova(gpp.null,gpp.alt)
t(round(AICc(gpp.null,gpp.alt),2))# 0.0042

load(file="r_output/gpp_null")
plot(gpp.null)
load(file="r_output/gpp_alt")
plot(gpp.alt)





#npp
ter_prod.marg=ter_prod[!is.na(ter_prod$npp),]

k=ceiling(sqrt(dim(ter_prod.marg)[1]/500))
n.lat=length(unique(ter_prod.marg$Lat))
lat.sp=sort(unique(ter_prod.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(ter_prod.marg$Long))
long.sp=sort(unique(ter_prod.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
ter_prod.marg.sp=ter_prod.marg[ter_prod.marg$Lat%in%lat.sp&ter_prod.marg$Long%in%long.sp,]#subsample
boxplot(ter_prod.marg.sp$npp~ter_prod.marg.sp$continent)



AIC_npp=rep(NA,10)
for(k in 1:10){
  AIC_npp[k]=try(AIC(lme(fixed = npp ~ -1+aust, data = ter_prod.marg.sp,
                         random = ~ 1 | continent,
                         correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                         method = "ML")));
  if(class(AIC_npp[k]) == "try-error") next;
}
save(AIC_npp,file="~/is_australia_weird/r_output/AIC_npp")
best=which(AIC_npp==min(AIC_npp))
c(Type[best],Nug[best])

#npp

ter_prod.marg=ter_prod[!is.na(ter_prod$npp),]

k=ceiling(sqrt(dim(ter_prod.marg)[1]/10000))
n.lat=length(unique(ter_prod.marg$Lat))
lat.sp=sort(unique(ter_prod.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(ter_prod.marg$Long))
long.sp=sort(unique(ter_prod.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
ter_prod.marg.sp=ter_prod.marg[ter_prod.marg$Lat%in%lat.sp&ter_prod.marg$Long%in%long.sp,]#subsample
boxplot(ter_prod.marg.sp$npp~ter_prod.marg.sp$continent)

best=which(AIC_npp==min(AIC_npp))

npp.null <- lme(fixed = npp  ~ 1, data = ter_prod.marg.sp, random = ~ 1 | continent,
                method = "ML")




save(npp.null , file="~/is_australia_weird/r_output/npp_null")

npp.alt=lme(fixed = npp  ~ -1+aust, data = ter_prod.marg.sp,
            random = ~ 1 | continent,
            method = "ML")




save(npp.alt , file="~/is_australia_weird/r_output/npp_alt")

load(file="r_output/npp_null")
plot(gpp.null)
load(file="r_output/npp_alt")
plot(gpp.alt)


anova(npp.null,npp.alt)
t(round(AICc(npp.null,npp.alt),2))# 0.0113




