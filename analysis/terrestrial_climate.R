library(nlme)
library(lme4)
library(MCMCglmm)
library(spBayes)





climate=read.csv("data_for_analysis/terrestrial_climate_data.csv",header=T)

colnames(climate)[1:3]=c("continent","Lat", "Long")
datcols=c(4:14)
climate$continent=factor(climate$continent)
climate$aust=climate$continent=="Australia" 

vars=colnames(climate[datcols])
Type=rep(c("spherical", "exponential", "gaussian", "linear","rational"),2)
Nug=rep(c(TRUE,FALSE),each=5)

#univariate
#dtr
climate.marg=climate[!is.na(climate$dtr),]

k=ceiling(sqrt(dim(climate.marg)[1]/500))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(log(climate.marg.sp$dtr+1)~climate.marg.sp$continent)



AIC_dtr=rep(NA,10)
for(k in 1:10){
  AIC_dtr[k]=try(AIC(lme(fixed = dtr ~ -1+aust, data = climate.marg.sp,
                              random = ~ 1 | continent,
                              correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                              method = "ML")));
  if(class(AIC_dtr[k]) == "try-error") next;
}
save(AIC_dtr,file="r_output/AIC_dtr")
best=which(AIC_dtr==min(AIC_dtr))
c(Type[best],Nug[best])

#dtr

climate.marg=climate[!is.na(climate$dtr),]

k=ceiling(sqrt(dim(climate.marg)[1]/2000))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(log(climate.marg.sp$dtr+1)~climate.marg.sp$continent)

best=which(AIC_dtr==min(AIC_dtr))

dtr.null <- lme(fixed = dtr  ~ 1, data = climate.marg.sp, random = ~ 1 | continent,
                     correlation = corSpatial( form = ~ Lat + Long| continent,
                                               nugget=Nug[best],type=Type[best]),
                     method = "ML")




save(dtr.null , file="r_output/dtr_null")

dtr.alt=lme(fixed = dtr  ~ -1+aust, data = climate.marg.sp,
                 random = ~ 1 | continent,
                 correlation = corSpatial( form = ~ Lat + Long| continent,
                                           nugget=Nug[best],type=Type[best]),
                 method = "ML")




save(dtr.alt , file="r_output/dtr_alt")


load(file="r_output/dtr_null")
plot(dtr.null)
load(file="r_output/dtr_alt")
plot(dtr.alt)

anova(dtr.null,dtr.alt)# 0.0789
t(round(AICc(dtr.null,dtr.alt),2))
#frs
climate.marg=climate[!is.na(climate$frs),]
climate.marg$logfrs=log(climate.marg$frs+1)
k=ceiling(sqrt(dim(climate.marg)[1]/500))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(log(climate.marg.sp$frs+1)~climate.marg.sp$continent)



AIC_frs=rep(NA,10)
for(k in 1:10){
  AIC_frs[k]=try(AIC(lme(fixed = logfrs ~ -1+aust, data = climate.marg.sp,
                         random = ~ 1 | continent,
                         correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                         method = "ML")));
  if(class(AIC_frs[k]) == "try-error") next;
}
save(AIC_frs,file="r_output/AIC_frs")
best=which(AIC_frs==min(AIC_frs))
c(Type[best],Nug[best])

#frs

climate.marg=climate[!is.na(climate$frs),]
climate.marg$logfrs=log(climate.marg$frs+1) #log +1 transformed since residual plots were quite bad

k=ceiling(sqrt(dim(climate.marg)[1]/2000))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(log(climate.marg.sp$frs+1)~climate.marg.sp$continent)

best=which(AIC_frs==min(AIC_frs))

frs.null <- lme(fixed = logfrs  ~ 1, data = climate.marg.sp, random = ~ 1 | continent,
                correlation = corSpatial( form = ~ Lat + Long| continent,
                                          nugget=Nug[best],type=Type[best]),
                method = "ML")




save(frs.null , file="r_output/frs_null")

frs.alt=lme(fixed = logfrs  ~ -1+aust, data = climate.marg.sp,
            random = ~ 1 | continent,
            correlation = corSpatial( form = ~ Lat + Long| continent,
                                      nugget=Nug[best],type=Type[best]),
            method = "ML")




save(frs.alt , file="r_output/frs_alt")

anova(frs.null,frs.alt)# 0.2639

load(file="r_output/frs_null")
plot(frs.null)
load(file="r_output/frs_alt")
plot(frs.alt)
t(round(AICc(frs.null,frs.alt),2))
#pre
climate.marg=climate[!is.na(climate$pre),]

k=ceiling(sqrt(dim(climate.marg)[1]/500))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$pre~climate.marg.sp$continent)



AIC_pre=rep(NA,10)
for(k in 1:10){
  AIC_pre[k]=try(AIC(lme(fixed = pre ~ -1+aust, data = climate.marg.sp,
                         random = ~ 1 | continent,
                         correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                         method = "ML")));
  if(class(AIC_pre[k]) == "try-error") next;
}
save(AIC_pre,file="r_output/AIC_pre")
best=which(AIC_pre==min(AIC_pre))
c(Type[best],Nug[best])

#pre

climate.marg=climate[!is.na(climate$pre),]

k=ceiling(sqrt(dim(climate.marg)[1]/2000))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$pre~climate.marg.sp$continent)

best=which(AIC_pre==min(AIC_pre))

pre.null <- lme(fixed = pre  ~ 1, data = climate.marg.sp, random = ~ 1 | continent,
                correlation = corSpatial( form = ~ Lat + Long| continent,
                                          nugget=Nug[best],type=Type[best]),
                method = "ML")




save(pre.null , file="r_output/pre_null")

pre.alt=lme(fixed = pre  ~ -1+aust, data = climate.marg.sp,
            random = ~ 1 | continent,
            correlation = corSpatial( form = ~ Lat + Long| continent,
                                      nugget=Nug[best],type=Type[best]),
            method = "ML")




save(pre.alt , file="r_output/pre_alt")

anova(pre.null,pre.alt)# 0.5263

load(file="r_output/pre_null")
plot(pre.null)
load(file="r_output/pre_alt")
plot(pre.alt)
t(round(AICc(pre.null,pre.alt),2))
#sunp
climate.marg=climate[!is.na(climate$sunp),]

k=ceiling(sqrt(dim(climate.marg)[1]/500))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$sunp~climate.marg.sp$continent)



AIC_sunp=rep(NA,10)
for(k in 1:10){
  AIC_sunp[k]=try(AIC(lme(fixed = sunp ~ -1+aust, data = climate.marg.sp,
                          random = ~ 1 | continent,
                          correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                          method = "ML")));
  if(class(AIC_sunp[k]) == "try-error") next;
}
save(AIC_sunp,file="r_output/AIC_sunp")
best=which(AIC_sunp==min(AIC_sunp))
c(Type[best],Nug[best])

#sunp

climate.marg=climate[!is.na(climate$sunp),]

k=ceiling(sqrt(dim(climate.marg)[1]/2000))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$sunp~climate.marg.sp$continent)

best=which(AIC_sunp==min(AIC_sunp))

sunp.null <- lme(fixed = sunp  ~ 1, data = climate.marg.sp, random = ~ 1 | continent,
                 correlation = corSpatial( form = ~ Lat + Long| continent,
                                           nugget=Nug[best],type=Type[best]),
                 method = "ML")




save(sunp.null , file="r_output/sunp_null")

sunp.alt=lme(fixed = sunp  ~ -1+aust, data = climate.marg.sp,
             random = ~ 1 | continent,
             correlation = corSpatial( form = ~ Lat + Long| continent,
                                       nugget=Nug[best],type=Type[best]),
             method = "ML")




save(sunp.alt , file="r_output/sunp_alt")

anova(sunp.null,sunp.alt)# 0.1444

load(file="r_output/sunp_null")
plot(sunp.null)
load(file="r_output/sunp_alt")
plot(sunp.alt)
t(round(AICc(sunp.null,sunp.alt),2))

#tmp
climate.marg=climate[!is.na(climate$tmp),]

k=ceiling(sqrt(dim(climate.marg)[1]/500))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$tmp~climate.marg.sp$continent)



AIC_tmp=rep(NA,10)
for(k in 1:10){
  AIC_tmp[k]=try(AIC(lme(fixed = tmp ~ -1+aust, data = climate.marg.sp,
                         random = ~ 1 | continent,
                         correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                         method = "ML")));
  if(class(AIC_tmp[k]) == "try-error") next;
}
save(AIC_tmp,file="r_output/AIC_tmp")
best=which(AIC_tmp==min(AIC_tmp))
c(Type[best],Nug[best])

#tmp

climate.marg=climate[!is.na(climate$tmp),]

k=ceiling(sqrt(dim(climate.marg)[1]/2000))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$tmp~climate.marg.sp$continent)

best=which(AIC_tmp==min(AIC_tmp))

tmp.null <- lme(fixed = tmp  ~ 1, data = climate.marg.sp, random = ~ 1 | continent,
                correlation = corSpatial( form = ~ Lat + Long| continent,
                                          nugget=Nug[best],type=Type[best]),
                method = "ML")




save(tmp.null , file="r_output/tmp_null")

tmp.alt=lme(fixed = tmp  ~ -1+aust, data = climate.marg.sp,
            random = ~ 1 | continent,
            correlation = corSpatial( form = ~ Lat + Long| continent,
                                      nugget=Nug[best],type=Type[best]),
            method = "ML")




save(tmp.alt , file="r_output/tmp_alt")

anova(tmp.null,tmp.alt)# 0.4372

load(file="r_output/tmp_null")
plot(tmp.null)
load(file="r_output/tmp_alt")
plot(tmp.alt)
t(round(AICc(tmp.null,tmp.alt),2))
#wnd
climate.marg=climate[!is.na(climate$wnd),]

k=ceiling(sqrt(dim(climate.marg)[1]/500))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$wnd~climate.marg.sp$continent)



AIC_wnd=rep(NA,10)
for(k in 1:10){
  AIC_wnd[k]=try(AIC(lme(fixed = wnd ~ -1+aust, data = climate.marg.sp,
                         random = ~ 1 | continent,
                         correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                         method = "ML")));
  if(class(AIC_wnd[k]) == "try-error") next;
}
save(AIC_wnd,file="r_output/AIC_wnd")
best=which(AIC_wnd==min(AIC_wnd))
c(Type[best],Nug[best])

#wnd

climate.marg=climate[!is.na(climate$wnd),]

k=ceiling(sqrt(dim(climate.marg)[1]/2000))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$wnd~climate.marg.sp$continent)

best=which(AIC_wnd==min(AIC_wnd))

wnd.null <- lme(fixed = wnd  ~ 1, data = climate.marg.sp, random = ~ 1 | continent,
                correlation = corSpatial( form = ~ Lat + Long| continent,
                                          nugget=Nug[best],type=Type[best]),
                method = "ML")




save(wnd.null , file="r_output/wnd_null")

wnd.alt=lme(fixed = wnd  ~ -1+aust, data = climate.marg.sp,
            random = ~ 1 | continent,
            correlation = corSpatial( form = ~ Lat + Long| continent,
                                      nugget=Nug[best],type=Type[best]),
            method = "ML")




save(wnd.alt , file="r_output/wnd_alt")

anova(wnd.null,wnd.alt)# 0.3068

load(file="r_output/wnd_null")
plot(wnd.null)
load(file="r_output/wnd_alt")
plot(wnd.alt)
t(round(AICc(wnd.null,wnd.alt),2))
#pre.cv
climate.marg=climate[!is.na(climate$pre.cv),]

k=ceiling(sqrt(dim(climate.marg)[1]/500))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$pre.cv~climate.marg.sp$continent) #log transformed due to yucky residual plots

boxplot(log(climate.marg.sp$pre.cv)~climate.marg.sp$continent)
climate.marg.sp$logpre.cv=log(climate.marg.sp$pre.cv)

boxplot(climate.marg.sp$logpre~climate.marg.sp$continent)



AIC_pre.cv=rep(NA,10)
for(k in 1:10){
  AIC_pre.cv[k]=try(AIC(lme(fixed = logpre.cv ~ -1+aust, data = climate.marg.sp,
                            random = ~ 1 | continent,
                            correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                            method = "ML")));
  if(class(AIC_pre.cv[k]) == "try-error") next;
}
save(AIC_pre.cv,file="r_output/AIC_pre.cv")
best=which(AIC_pre.cv==min(AIC_pre.cv))
c(Type[best],Nug[best])

#pre.cv

climate.marg=climate[!is.na(climate$pre.cv),]

k=ceiling(sqrt(dim(climate.marg)[1]/2000))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$pre.cv~climate.marg.sp$continent)
climate.marg.sp$logpre.cv=log(climate.marg.sp$pre.cv)
boxplot(climate.marg.sp$logpre~climate.marg.sp$continent)

best=which(AIC_pre.cv==min(AIC_pre.cv))

pre.cv.null <- lme(fixed = logpre.cv  ~ 1, data = climate.marg.sp, random = ~ 1 | continent,
                   correlation = corSpatial( form = ~ Lat + Long| continent,
                                             nugget=Nug[best],type=Type[best]),
                   method = "ML")




save(pre.cv.null , file="r_output/pre.cv_null")

pre.cv.alt=lme(fixed = logpre.cv  ~ -1+aust, data = climate.marg.sp,
               random = ~ 1 | continent,
               correlation = corSpatial( form = ~ Lat + Long| continent,
                                         nugget=Nug[best],type=Type[best]),
               method = "ML")




save(pre.cv.alt , file="r_output/pre.cv_alt")

anova(pre.cv.null,pre.cv.alt)# 0.6675

load(file="r_output/pre.cv_null")
plot(pre.cv.null)
load(file="r_output/pre.cv_alt")
plot(pre.cv.alt)
t(round(AICc(pre.cv.null,pre.cv.alt),2))
#nrd.dm
climate.marg=climate[!is.na(climate$nrd.dm),]

k=ceiling(sqrt(dim(climate.marg)[1]/500))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$nrd.dm~climate.marg.sp$continent)



AIC_nrd.dm=rep(NA,10)
for(k in 1:10){
  AIC_nrd.dm[k]=try(AIC(lme(fixed = nrd.dm ~ -1+aust, data = climate.marg.sp,
                            random = ~ 1 | continent,
                            correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                            method = "ML")));
  if(class(AIC_nrd.dm[k]) == "try-error") next;
}
save(AIC_nrd.dm,file="r_output/AIC_nrd.dm")
best=which(AIC_nrd.dm==min(AIC_nrd.dm))
c(Type[best],Nug[best])

#nrd.dm

climate.marg=climate[!is.na(climate$nrd.dm),]

k=ceiling(sqrt(dim(climate.marg)[1]/2000))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$nrd.dm~climate.marg.sp$continent)

best=which(AIC_nrd.dm==min(AIC_nrd.dm))

nrd.dm.null <- lme(fixed = nrd.dm  ~ 1, data = climate.marg.sp, random = ~ 1 | continent,
                   correlation = corSpatial( form = ~ Lat + Long| continent,
                                             nugget=Nug[best],type=Type[best]),
                   method = "ML")




save(nrd.dm.null , file="r_output/nrd.dm_null")

nrd.dm.alt=lme(fixed = nrd.dm  ~ -1+aust, data = climate.marg.sp,
               random = ~ 1 | continent,
               correlation = corSpatial( form = ~ Lat + Long| continent,
                                         nugget=Nug[best],type=Type[best]),
               method = "ML")




save(nrd.dm.alt , file="r_output/nrd.dm_alt")

anova(nrd.dm.null,nrd.dm.alt)# 0.6385 #rainy days driest month

load(file="r_output/nrd.dm_null")
plot(nrd.dm.null)
load(file="r_output/nrd.dm_alt")
plot(nrd.dm.alt)
t(round(AICc(nrd.dm.null,nrd.dm.alt),2))

#max.temp
climate.marg=climate[!is.na(climate$max.temp),]

k=ceiling(sqrt(dim(climate.marg)[1]/500))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$max.temp~climate.marg.sp$continent)



AIC_max.temp=rep(NA,10)
for(k in 1:10){
  AIC_max.temp[k]=try(AIC(lme(fixed = max.temp ~ -1+aust, data = climate.marg.sp,
                              random = ~ 1 | continent,
                              correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                              method = "ML")));
  if(class(AIC_max.temp[k]) == "try-error") next;
}
save(AIC_max.temp,file="r_output/AIC_max.temp")
best=which(AIC_max.temp==min(AIC_max.temp))
c(Type[best],Nug[best])

#max.temp

climate.marg=climate[!is.na(climate$max.temp),]

k=ceiling(sqrt(dim(climate.marg)[1]/2000))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(climate.marg.sp$max.temp~climate.marg.sp$continent)

best=which(AIC_max.temp==min(AIC_max.temp))

max.temp.null <- lme(fixed = max.temp  ~ 1, data = climate.marg.sp, random = ~ 1 | continent,
                     correlation = corSpatial( form = ~ Lat + Long| continent,
                                               nugget=Nug[best],type=Type[best]),
                     method = "ML")




save(max.temp.null , file="r_output/max.temp_null")

max.temp.alt=lme(fixed = max.temp  ~ -1+aust, data = climate.marg.sp,
                 random = ~ 1 | continent,
                 correlation = corSpatial( form = ~ Lat + Long| continent,
                                           nugget=Nug[best],type=Type[best]),
                 method = "ML")




save(max.temp.alt , file="r_output/max.temp_alt")

anova(max.temp.null,max.temp.alt)# 0.5542

load(file="r_output/max.temp_null")
plot(max.temp.null)
load(file="r_output/max.temp_alt")
plot(max.temp.alt)
t(round(AICc(max.temp.null,max.temp.alt),2))
#rd0
climate.marg=climate[!is.na(climate$rd0),]

k=ceiling(sqrt(dim(climate.marg)[1]/500))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(log(climate.marg.sp$rd0+1)~climate.marg.sp$continent)



AIC_rd0=rep(NA,10)
for(k in 1:10){
  AIC_rd0[k]=try(AIC(lme(fixed = rd0 ~ -1+aust, data = climate.marg.sp,
                         random = ~ 1 | continent,
                         correlation = corSpatial( form = ~ Lat + Long| continent,nugget=Nug[k],type=Type[k]),
                         method = "ML")));
  if(class(AIC_rd0[k]) == "try-error") next;
}
save(AIC_rd0,file="r_output/AIC_rd0")
best=which(AIC_rd0==min(AIC_rd0))
c(Type[best],Nug[best])

#rd0

climate.marg=climate[!is.na(climate$rd0),]

k=ceiling(sqrt(dim(climate.marg)[1]/2000))
n.lat=length(unique(climate.marg$Lat))
lat.sp=sort(unique(climate.marg$Lat))[floor(seq(1,n.lat,length=ceiling(n.lat/k)))]
n.long=length(unique(climate.marg$Long))
long.sp=sort(unique(climate.marg$Long))[floor(seq(1,n.long,length=ceiling(n.long/k)))]
climate.marg.sp=climate.marg[climate.marg$Lat%in%lat.sp&climate.marg$Long%in%long.sp,]#subsample
boxplot(log(climate.marg.sp$rd0+1)~climate.marg.sp$continent)

best=which(AIC_rd0==min(AIC_rd0))

rd0.null <- lme(fixed = rd0  ~ 1, data = climate.marg.sp, random = ~ 1 | continent,
                correlation = corSpatial( form = ~ Lat + Long| continent,
                                          nugget=Nug[best],type=Type[best]),
                method = "ML")




save(rd0.null , file="r_output/rd0_null")

rd0.alt=lme(fixed = rd0  ~ -1+aust, data = climate.marg.sp,
            random = ~ 1 | continent,
            correlation = corSpatial( form = ~ Lat + Long| continent,
                                      nugget=Nug[best],type=Type[best]),
            method = "ML")




save(rd0.alt , file="r_output/rd0_alt")


load(file="r_output/rd0_null")
plot(rd0.null)
load(file="r_output/rd0_alt")
plot(rd0.alt)

anova(rd0.null,rd0.alt)# 0.0789
t(round(AICc(rd0.null,rd0.alt),2))



