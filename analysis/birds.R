#read in data
library(nlme)
library(lme4)

#Birds
birds=read.csv("data_for_analysis/birds_data.csv")
#plot data
datcols=3:7
order=sample(1:dim(birds)[1]) #so we don't plot continents in order
pairs(birds[order,datcols],col=birds$continent[order],pch=20,cex=1.2)
#colors by continent
rbind(levels(birds$continent),palette()[1:length(levels(birds$continent))])




#tranform data into columns
n.var=length(datcols)
continent=factor(rep(birds$continent,n.var))
variable=factor(rep(colnames(birds)[datcols],each=dim(birds)[1]))
aust=continent=="Australia"
id=rep(1:dim(birds)[1],n.var)
value=as.vector(as.matrix(birds[,datcols]))
bir=data.frame(value=value,aust=aust,variable=variable,continent=continent,id=id)
bir=bir[!is.na(bir$value),] #we remove missing data
bir$v <- as.numeric(bir$variable) #we need this later for lme



#univariate models for birda data

#"aa_f_maturity"

bir.marg=bir[bir$variable==levels(bir$variable)[1],]
marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = bir.marg,method="ML")
marg.alt<- lme(value ~ -1+aust , random = ~ 1| continent , data = bir.marg,method="ML")
summary(marg.null )
summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt) #p=0.1132
t(AICc(marg.null,marg.alt))

#"aa_growth_rate"

bir.marg=bir[bir$variable==levels(bir$variable)[2],]
marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = bir.marg,method="ML")
marg.alt<- lme(value ~ -1+aust , random = ~ 1| continent , data = bir.marg,method="ML")
# summary(marg.null )
# summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt) #p=0.0126
t(round(AICc(marg.null,marg.alt),2))

#"aa_max_longevity"

bir.marg=bir[bir$variable==levels(bir$variable)[3],]
marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = bir.marg,method="ML")
marg.alt<- lme(value ~ -1+aust , random = ~ 1| continent , data = bir.marg,method="ML")
# summary(marg.null )
# summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt) #p=0.0227
t(round(AICc(marg.null,marg.alt),2))


#"f_mass"

bir.marg=bir[bir$variable==levels(bir$variable)[4],]
marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = bir.marg,method="ML")
marg.alt<- lme(value ~ -1+aust , random = ~ 1| continent , data = bir.marg,method="ML")
# summary(marg.null )
# summary(marg.alt )

plot(marg.null ) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt) #0.4594
t(round(AICc(marg.null,marg.alt),2))



#"hatch_prod_g"

bir.marg=bir[bir$variable==levels(bir$variable)[5],]
marg.null<- lme(value ~ 1 , random = ~ 1| continent , data = bir.marg,method="ML")
marg.alt<- lme(value ~ -1+aust , random = ~ 1| continent , data = bir.marg,method="ML")
# summary(marg.null )
# summary(marg.alt )

plot(marg.null) #OK
plot(marg.alt ) #OK
anova(marg.null,marg.alt) #0.7862
t(round(AICc(marg.null,marg.alt),2))
