library(nlme)
library(lme4)
library(MCMCglmm)
library(spBayes)


soil_n=read.csv("~/Projects/is_australia_weird/data_for_analysis/soil_total_n_data.csv",header=T)

#soil N
#plot data
datcols=4




#tranform data into columns
n.var=length(datcols)
continent=factor(rep(soil_n$continent,n.var))
variable=factor(rep(colnames(soil_n)[datcols],each=dim(soil_n)[1]))
aust=continent=="Australia"
id=rep(1:dim(soil_n)[1],n.var)
value=as.vector(as.matrix(soil_n[,datcols]))
soiln=data.frame(value=value,aust=aust,variable=variable,continent=continent,id=id)
soiln=soiln[!is.na(soiln$value),] #we remove missing data
soiln$v <- as.numeric(soiln$variable) #we need this later for lme


soiln.marg=soiln[soiln$variable==levels(soiln$variable)[1],]
total_n.null<- lme(log10(value) ~ 1 , random = ~ 1| continent , data = soiln.marg,method="ML")
total_n.alt<- lme(log10(value) ~ -1+aust , random = ~ 1| continent , data = soiln.marg,method="ML")
summary(total_n.null )
summary(total_n.alt)

plot(total_n.null) #OK
plot(total_n.alt) #OK
anova(total_n.null,total_n.alt) #p=0.1034
t(round(AICc(total_n.null,total_n.alt),2))

