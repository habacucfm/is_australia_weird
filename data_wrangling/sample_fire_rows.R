library(fst)
library(tidyfst)
library(tidyverse)
#Terrestrial data
fire00 <- read_fst("fire_00ssump.fst", columns = c("continent","x","y","value"))
fire01 <- read_fst("fire_01ssump.fst", columns = c("value"))
fire02 <- read_fst("fire_02ssump.fst", columns = c("value"))
fire03 <- read_fst("fire_03ssump.fst", columns = c("value"))
fire04 <- read_fst("fire_04ssump.fst", columns = c("value"))
fire05 <- read_fst("fire_05ssump.fst", columns = c("value"))
fire06 <- read_fst("fire_06ssump.fst", columns = c("value"))
fire07 <- read_fst("fire_07ssump.fst", columns = c("value"))
fire08 <- read_fst("fire_08ssump.fst", columns = c("value"))
fire09 <- read_fst("fire_09ssump.fst", columns = c("value"))
fire10 <- read_fst("fire_10ssump.fst", columns = c("value"))
fire11 <- read_fst("fire_11ssump.fst", columns = c("value"))
fire12 <- read_fst("fire_12ssump.fst", columns = c("value"))
fire13 <- read_fst("fire_13ssump.fst", columns = c("value"))
fire14 <- read_fst("fire_14ssump.fst", columns = c("value"))
fire15 <- read_fst("fire_15ssump.fst", columns = c("value"))
fire16 <- read_fst("fire_16ssump.fst", columns = c("value"))
fire17 <- read_fst("fire_17ssump.fst", columns = c("value"))
fire18 <- read_fst("fire_18ssump.fst", columns = c("value"))
fire19 <- read_fst("fire_19ssump.fst", columns = c("value"))


fire <- bind_cols(fire00 ,fire01 ,fire02 ,fire03 ,fire04 ,fire05 
                  ,fire06 ,fire07 ,fire08 ,fire09 ,fire10 ,fire11 ,
                  fire12 ,fire13 ,fire14 ,fire15, fire16, fire17, fire18, fire19)

rm(fire00 ,fire01 ,fire02 ,fire03 ,fire04 ,fire05 
          ,fire06 ,fire07 ,fire08 ,fire09 ,fire10 ,fire11 ,
          fire12 ,fire13 ,fire14 ,fire15, fire16, fire17, fire18, fire19)
gc()
fire %>%
  tidyfst::sample_frac_dt(size = 0.10, replace = FALSE) %>%
  replace_na_dt(to = 0) %>%
  mutate_(sumfire = rowSums(.[,4:23]))%>% 
  tidyfst::select_dt(continent, x, y, sumfire) -> fire
  
write_fst(fire, "fire.fst")
