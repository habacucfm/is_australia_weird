#This scripts takes .txt files that contain 'NaN' as 'NA' values and 
#space characters at the begging of the line and between columns and
# then coverts the files into .csv files whipes out the spaces
#and replaces them for commas (at least btw columns) and finally
#replaces NaN values for nothing (i.e."").
#This scrips has only been tried in bash, THIS SCRIPT HAS TO BE 
# IN SAME FOLDER AS DATA TO BE RUN or has be made executable by adding
# the shebang below and given the right permissions (chmod u+x txt2csv.sh)
#!# /bin/bash
for datafile in *.txt
	do
	sed -e 's/^[ \t]*//' $datafile|
	sed 's/ \+/,/g'|
	sed 's/NaN//g'>${datafile%.txt}.csv;
done
#for datafile in *.txt
#	do
#	echo $datafile
#	sed -e 's/^[ \t]*//'|sed 's/ \+/,/g'|sed 's/NaN//g' >$datafile.csv
#done
