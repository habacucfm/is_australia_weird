#fire 
library(raster)
library(data.table)
library(sf)
library(exactextractr)
library(fst)
#polygon to use for raster data extraction
landWGS84<-st_read("raw_data/shapefiles/terrestrial/terrestrial_WGS84_GCS.shp")
no.islands<-landWGS84[landWGS84$area_sq_km > 1000000,]
#fix greenland
no.islands$continent[no.islands$ORIG_FID == 1] <- "North America"
fire_00<- raster("raw_data/sum2000.tif")
fire00subsamp <- exact_extract(fire_00,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire00subsamp) <- no.islands$continent
fire00subsamp <- rbindlist(fire00subsamp,idcol = "continent")
fire00subsamp <- fire00subsamp[fire00subsamp$coverage_fraction==1,]
write.fst(fire00subsamp, "fire_00ssump.fst")
rm(fire_00,fire00subsamp)
gc()

fire_01<- raster("raw_data/sum2001.tif")
fire01subsamp <- exact_extract(fire_01,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire01subsamp) <- no.islands$continent
fire01subsamp <- rbindlist(fire01subsamp, idcol = "continent")
fire01subsamp <- fire01subsamp[fire01subsamp$coverage_fraction==1,]
write.fst(fire01subsamp, "fire_01ssump.fst")
rm(fire_01,fire01subsamp)
gc()

fire_02<- raster("raw_data/sum2002.tif")
fire02subsamp <- exact_extract(fire_02,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire02subsamp) <- no.islands$continent
fire02subsamp <- rbindlist(fire02subsamp, idcol = "continent")
fire02subsamp <- fire02subsamp[fire02subsamp$coverage_fraction==1,]
write.fst(fire02subsamp, "fire_02ssump.fst")
rm(fire_02,fire02subsamp)
gc()

fire_03<- raster("raw_data/sum2003.tif")
fire03subsamp <- exact_extract(fire_03,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire03subsamp) <- no.islands$continent
fire03subsamp <- rbindlist(fire03subsamp, idcol = "continent")
fire03subsamp <- fire03subsamp[fire03subsamp$coverage_fraction==1,]
write.fst(fire03subsamp, "fire_03ssump.fst")
rm(fire_03,fire03subsamp)
gc()

fire_04<- raster("raw_data/sum2004.tif")

fire04subsamp <- exact_extract(fire_04,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire04subsamp) <- no.islands$continent
fire04subsamp <- rbindlist(fire04subsamp, idcol = "continent")
fire04subsamp <- fire04subsamp[fire04subsamp$coverage_fraction==1,]
write.fst(fire04subsamp, "fire_04ssump.fst")
rm(fire_04,fire04subsamp)
gc()

fire_05<- raster("raw_data/sum2005.tif")

fire05subsamp <- exact_extract(fire_05,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire05subsamp) <- no.islands$continent
fire05subsamp <- rbindlist(fire05subsamp, idcol = "continent")
fire05subsamp <- fire05subsamp[fire05subsamp$coverage_fraction==1,]
write.fst(fire05subsamp, "fire_05ssump.fst")
rm(fire_05,fire05subsamp)
gc()

fire_06<- raster("raw_data/sum2006.tif")

fire06subsamp <- exact_extract(fire_06,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire06subsamp) <- no.islands$continent
fire06subsamp <- rbindlist(fire06subsamp, idcol = "continent")
fire06subsamp <- fire06subsamp[fire06subsamp$coverage_fraction==1,]
write.fst(fire06subsamp, "fire_06ssump.fst")
rm(fire_06,fire06subsamp)
gc()

fire_07<- raster("raw_data/sum2007.tif")

fire07subsamp <- exact_extract(fire_07,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire07subsamp) <- no.islands$continent
fire07subsamp <- rbindlist(fire07subsamp, idcol = "continent")
fire07subsamp <- fire07subsamp[fire07subsamp$coverage_fraction==1,]
write.fst(fire07subsamp, "fire_07ssump.fst")
rm(fire_07,fire07subsamp)
gc()

fire_08<- raster("raw_data/sum2008.tif")

fire08subsamp <- exact_extract(fire_08,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire08subsamp) <- no.islands$continent
fire08subsamp <- rbindlist(fire08subsamp, idcol = "continent")
fire08subsamp <- fire08subsamp[fire08subsamp$coverage_fraction==1,]
write.fst(fire08subsamp, "fire_08ssump.fst")
rm(fire_08,fire08subsamp)
gc()

fire_09<- raster("raw_data/sum2009.tif")

fire09subsamp <- exact_extract(fire_09,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire09subsamp) <- no.islands$continent
fire09subsamp <- rbindlist(fire09subsamp, idcol = "continent")
fire09subsamp <- fire09subsamp[fire09subsamp$coverage_fraction==1,]
write.fst(fire09subsamp, "fire_09ssump.fst")
rm(fire_09,fire09subsamp)
gc()

fire_10<- raster("raw_data/sum2010.tif")

fire10subsamp <- exact_extract(fire_10,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire10subsamp) <- no.islands$continent
fire10subsamp <- rbindlist(fire10subsamp, idcol = "continent")
fire10subsamp <- fire10subsamp[fire10subsamp$coverage_fraction==1,]
write.fst(fire10subsamp, "fire_10ssump.fst")
rm(fire_10,fire10subsamp)
gc()

fire_11<- raster("raw_data/sum2011.tif")

fire11subsamp <- exact_extract(fire_11,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire11subsamp) <- no.islands$continent
fire11subsamp <- rbindlist(fire11subsamp, idcol = "continent")
fire11subsamp <- fire11subsamp[fire11subsamp$coverage_fraction==1,]
write.fst(fire11subsamp, "fire_11ssump.fst")
rm(fire_11,fire11subsamp)
gc()

fire_12<- raster("raw_data/sum2012.tif")

fire12subsamp <- exact_extract(fire_12,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire12subsamp) <- no.islands$continent
fire12subsamp <- rbindlist(fire12subsamp, idcol = "continent")
fire12subsamp <- fire12subsamp[fire12subsamp$coverage_fraction==1,]
write.fst(fire12subsamp, "fire_12ssump.fst")
rm(fire_12,fire12subsamp)
gc()

fire_13<- raster("raw_data/sum2013.tif")

fire13subsamp <- exact_extract(fire_13,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire13subsamp) <- no.islands$continent
fire13subsamp <- rbindlist(fire13subsamp, idcol = "continent")
fire13subsamp <- fire13subsamp[fire13subsamp$coverage_fraction==1,]
write.fst(fire13subsamp, "fire_13ssump.fst")
rm(fire_13,fire13subsamp)
gc()

fire_14<- raster("raw_data/sum2014.tif")

fire14subsamp <- exact_extract(fire_14,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire14subsamp) <- no.islands$continent
fire14subsamp <- rbindlist(fire14subsamp, idcol = "continent")
fire14subsamp <- fire14subsamp[fire14subsamp$coverage_fraction==1,]
write.fst(fire14subsamp, "fire_14ssump.fst")
rm(fire_14,fire14subsamp)
gc()

fire_15<- raster("raw_data/sum2015.tif")

fire15subsamp <- exact_extract(fire_15,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire15subsamp) <- no.islands$continent
fire15subsamp <- rbindlist(fire15subsamp, idcol = "continent")
fire15subsamp <- fire15subsamp[fire15subsamp$coverage_fraction==1,]
write.fst(fire15subsamp, "fire_15ssump.fst")
rm(fire_15,fire15subsamp)
gc()

fire_16<- raster("raw_data/sum2016.tif")

fire16subsamp <- exact_extract(fire_16,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire16subsamp) <- no.islands$continent
fire16subsamp <- rbindlist(fire16subsamp, idcol = "continent")
fire16subsamp <- fire16subsamp[fire16subsamp$coverage_fraction==1,]
write.fst(fire16subsamp, "fire_16ssump.fst")
rm(fire_16,fire16subsamp)
gc()

fire_17<- raster("raw_data/sum2017.tif")

fire17subsamp <- exact_extract(fire_17,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire17subsamp) <- no.islands$continent
fire17subsamp <- rbindlist(fire17subsamp, idcol = "continent")
fire17subsamp <- fire17subsamp[fire17subsamp$coverage_fraction==1,]
write.fst(fire17subsamp, "fire_17ssump.fst")
rm(fire_17,fire17subsamp)
gc()

fire_18<- raster("raw_data/sum2018.tif")

fire18subsamp <- exact_extract(fire_18,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire18subsamp) <- no.islands$continent
fire18subsamp <- rbindlist(fire18subsamp, idcol = "continent")
fire18subsamp <- fire18subsamp[fire18subsamp$coverage_fraction==1,]
write.fst(fire18subsamp, "fire_18ssump.fst")
rm(fire_18,fire18subsamp)
gc()

fire_19<- raster("raw_data/sum2019.tif")

fire19subsamp <- exact_extract(fire_19,y = no.islands, include_xy = TRUE, progress = TRUE)
names(fire19subsamp) <- no.islands$continent
fire19subsamp <- rbindlist(fire19subsamp, idcol = "continent")
fire19subsamp <- fire19subsamp[fire19subsamp$coverage_fraction==1,]
write.fst(fire19subsamp, "fire_19ssump.fst")
rm(fire_19,fire19subsamp)
gc()
